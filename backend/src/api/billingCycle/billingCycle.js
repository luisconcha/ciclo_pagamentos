const resful   = require('node-restful')
const mongoose = resful.mongoose


const creditSchema = new mongoose.Schema({
    name : { type: String, required: true },
    value: { type: Number, min: 0, required: true }
})

const debitSchema = new mongoose.Schema({
    name  : { type: String, required: true },
    value : { type: Number, min: 0, required: [ true, 'Informe o valor do débito!' ] },
    status: { type: String, required: false, uppercase: true, enum: [ 'PAGO', 'PENDENTE', 'AGENDADO' ] }
})

const billingCycleSchema = new mongoose.Schema({
    name   : { type: String, required: true },
    month  : { type: Number, min: 1, max: 12, required: true },
    year   : { type: Number, min: 1960, max: 20100, require: true },
    credits: [ creditSchema ],
    debts  : [ debitSchema ]
})

module.exports = resful.model('BilligCycle', billingCycleSchema)